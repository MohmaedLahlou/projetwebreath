# ProjetWebreath

-------------------------------------------
 Site web : Webreathe-Test
 Site de monitoring de modules IOT  
-------------------------------------------
-------------------------------------------

  Pré-requis

1. Programme requis pour lancer le projet :

   -----------
  |   Xampp   |
   -----------	
		
2. Télécharger et importer le projet :

 ----------------------------------------------------------------------------------------------------------------------
| Telecharger le .zip du projet . Ensuite décompresser le projet dans le dossier htdocs existant dans le dossier xampp.|
 ----------------------------------------------------------------------------------------------------------------------


3. Partie serveur :
 --------------------------------------------------------------------------------------
| Ouvrir le panel de contrôle de Xampp et lancer un module Apache ainsi que MySQL :    |
| Coller le script existant dans le fichier Sql dans base de données dans phpMyAdmin   |
 --------------------------------------------------------------------------------------

-------------------------------------------
-------------------------------------------
 
 Démarrage

 -----------------------------------------------------------------------------------
|Lancer votre navigateur et rentrer cette adresse "http://localhost/Pro/Home.php" . |
 -----------------------------------------------------------------------------------

 Réalisé avec
 -------------
| Sublime Text|
 -------------
 
 Auteur
 -------------
| Lahlou Mohamed|
 -------------


